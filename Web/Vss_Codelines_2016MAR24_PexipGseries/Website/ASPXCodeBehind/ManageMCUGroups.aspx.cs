﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
* ZD 100040 Smart MCU
*/
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;

namespace ns_MyVRM
{
    public partial class ManageMCUGroups : System.Web.UI.Page
    {

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        #region protected Members

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Table tblNoGroups;
        protected System.Web.UI.WebControls.DataGrid dgGroups;
        protected System.Web.UI.WebControls.TextBox txtSortBy;
        protected System.Web.UI.WebControls.TextBox txtSGroupName;
        protected System.Web.UI.WebControls.TextBox txtSMember;
        protected System.Web.UI.WebControls.TextBox txtSDescription;
        #endregion

        #region Private Members

        public Boolean searchgroup = false;

        #endregion

        #region ManageMCUGroups
        public ManageMCUGroups()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                
                obj.AccessandURLConformityCheck("ManageMCUGroups.aspx", Request.Url.AbsoluteUri.ToLower()); 
                
                errLabel.Attributes.Add("style", "display:none");
                errLabel.Text = "";

                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Attributes.Add("style", "display:block");
                    }

                if (!IsPostBack)
                    BindDataWithInXML();

            }
            catch (Exception ex)
            {
                errLabel.Attributes.Add("style", "display:block");
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load" + ex.Message);
            }

        }
        #endregion

        #region BindDataWithInXML
        protected void BindDataWithInXML()
        {
            Session["multisiloOrganizationID"] = null;
            String inXML = "<GetMCUGroups>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><sortBy>" + txtSortBy.Text + "</sortBy></GetMCUGroups>";
            BindData(inXML, "GetMCUGroups");
        }
        #endregion

        #region BindData
        private void BindData(String inXML, String cmd)
        {
            try
            {
                String outXML = obj.CallMyVRMServer(cmd, inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//groups/group");

                    if (nodes.Count > 0)
                    {
                        LoadMCUGroupGrid(nodes);

                        //foreach (DataGridItem dgroup in dgGroups.Items)
                        //{
                        //    ((HtmlButton)dgroup.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:OpenDetails('" + dgroup.Cells[0].Text + "');return false;");
                        //}

                        dgGroups.Visible = true;
                        tblNoGroups.Visible = false;
                    }
                    else
                    {
                        dgGroups.Visible = false;
                        tblNoGroups.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Attributes.Add("style", "display:block"); 
                }
            }
            catch (Exception ex)
            {
                errLabel.Attributes.Add("style", "display:block");
                log.Trace("BindData" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region LoadMCUGroupGrid
        protected void LoadMCUGroupGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();
                             
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    dgGroups.DataSource = dt;
                    dgGroups.DataBind();
                }

            }
            catch (Exception ex)
            {
                log.Trace("LoadMCUGroupGrid" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Attributes.Add("style", "display:block");
            }
        }
        
        #endregion

        #region dgGroups_DataBound
        protected void dgGroups_DataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    if (row != null)
                    {
                        HtmlButton dgGrpDetails = (HtmlButton)e.Item.FindControl("btnViewDetails");
                        dgGrpDetails.Attributes.Add("onclick", "javascript:OpenDetails('" + row["groupName"].ToString() + "','" + row["mcustring"].ToString() + "');return false;");

                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region DeleteMCUGroup
        protected void DeleteMCUGroup(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML = "<DeleteMCUGroup>" +
                obj.OrgXMLElement() +
                "<userID>" + Session["userID"].ToString() + "</userID>" +
                "<groupID>" + e.Item.Cells[0].Text + "</groupID></DeleteMCUGroup>";

                String outXML = obj.CallMyVRMServer("DeleteMCUGroup", inXML, Application["COM_ConfigPath"].ToString()); //Need Change With MCU delete Name

                errLabel.Attributes.Add("style", "display:block");
                if (outXML.IndexOf("<error>") > 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    Response.Redirect("ManageMCUGroups.aspx?m=1");
                }
            }
            catch (Exception ex)
            {
                log.Trace("DeleteMCUGroup" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Attributes.Add("style", "display:block");
            }
        }
        #endregion

        #region EditMCUGroup
        protected void EditMCUGroup(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Response.Redirect("MCULoadbalanceGroup.aspx?groupID=" + e.Item.Cells[0].Text);

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("EditMCUgroup" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Attributes.Add("style", "display:block"); 
            }
        }
        #endregion

        #region SearchGroup
        protected void SearchGroup(Object sender, EventArgs e)
        {
            if ((txtSGroupName.Text == "") && (txtSMember.Text == "") && (txtSDescription.Text == ""))
            {
                BindDataWithInXML();
            }
            else
            {
                StringBuilder inXML = new StringBuilder();
                searchgroup = true;
                inXML.Append("<SearchMCUGroup>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<groupName>" + txtSGroupName.Text + "</groupName>");
                inXML.Append("<includedMCU>" + txtSMember.Text + "</includedMCU>");
                inXML.Append("<description>" + txtSDescription.Text + "</description>");
                inXML.Append("</SearchMCUGroup>");
                log.Trace(inXML.ToString());
                BindData(inXML.ToString(), "SearchMCUGroup");
                txtSGroupName.Text = "";
                txtSMember.Text = "";
                txtSDescription.Text = "";
            }


        }
        #endregion

        #region ResetGroup
        protected void ResetGroup(Object sender, EventArgs e)
        {
            Response.Redirect("ManageMCUGroups.aspx?opr=''&tid=''"); 
        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Group ?") + "')"); 
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindRowsDeleteMessage" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Attributes.Add("style", "display:block");
            }
        }
        #endregion

        #region CreateNewMCUGroup
        protected void CreateNewMCUGroup(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("MCULoadbalanceGroup.aspx");

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("CreateNewMCUGroup" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Attributes.Add("style", "display:block"); 
            }
        }
        #endregion
    }
}